import { Component } from "react";
import dice from "../assets/dice.png";
import dice1 from "../assets/1.png";
import dice2 from "../assets/2.png";
import dice3 from "../assets/3.png";
import dice4 from "../assets/4.png";
import dice5 from "../assets/5.png";
import dice6 from "../assets/6.png";

class Dice extends Component{
    constructor(props){
        super(props);
        this.state = {
            diceNumber: 0,
            src: dice,
        };
    }
     onBtnClick=(event)=>{
        const diceRandom = 1+ Math.floor(Math.random() * 6);//tạo bên ngoài tránh delay
        // componentDidUpdate(()=>{
        //     this.setState({
        //         diceNumber: 1+ Math.floor(Math.random() * 6)
        // })
         this.setState({
            diceNumber: diceRandom
        });

        console.log(diceRandom);//log được giá trị trực tiếp
        //dùng diceRandom thay đổi hình ảnh xúc xắc
        if(diceRandom === 1){
            this.setState({
                src: dice1
            })
        }
        if(diceRandom === 2){
            this.setState({
                src: dice2
            })
        }  
        if(diceRandom === 3){
            this.setState({
                src: dice3
            })
        }  
        if(diceRandom === 4){
            this.setState({
                src: dice4
            })
        }  
        if(diceRandom === 5){
            this.setState({
                src: dice5
            })
        }  
        if(diceRandom === 6){
            this.setState({
                src: dice6
            })
        }  
    }
   
    
    render(){
        return(
            <div style={{display:"flex",flexDirection: "column", alignItems:"center",marginTop:"5px", textAlign:"center"}}>
                <div>
                    <img src={this.state.src} style={{width:"300px"}}></img>
                    <p>Dice: {this.state.diceNumber}</p>
                </div>
                <div> 
                    <button onClick={this.onBtnClick}>Ném Xúc Xắc</button>
                </div>
            </div>
        )
    }
}
export default Dice;